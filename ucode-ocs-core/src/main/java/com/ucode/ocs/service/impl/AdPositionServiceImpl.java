package com.ucode.ocs.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ucode.ocs.mapper.AdPositionMapper;
import com.ucode.ocs.model.AdPosition;
import com.ucode.ocs.service.AdPositionService;
import com.ucode.tool.base.Paginator;
import com.ucode.tool.util.IdentifyUtils;

@Service
public class AdPositionServiceImpl implements AdPositionService {

    @Autowired
    private AdPositionMapper adPositionMapper;
    
    @Override
    public int insert(AdPosition adPosition) {
        Date now = new Date();
        adPosition.setId(IdentifyUtils.getDefaultSnowflakeId());
        adPosition.setCreatedTime(now);
        adPosition.setUpdatedTime(now);
        return adPositionMapper.insert(adPosition);
    }

    @Override
    public int update(AdPosition adPosition) {
        return adPositionMapper.update(adPosition);
    }

    @Override
    public int deleteById(Long id) {
        return adPositionMapper.deleteById(id);
    }

    @Override
    public AdPosition findById(Long id) {
        return adPositionMapper.findById(id);
    }

    @Override
    public List<AdPosition> findByIds(List<Long> ids) {
        return adPositionMapper.findByIds(ids);
    }

    @Override
    public Paginator<AdPosition> queryPage(String name, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AdPosition> list = adPositionMapper.queryList(name);
        PageInfo<AdPosition> pageInfo = new PageInfo<>(list);
        return new Paginator<>(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), list);
    }

}
