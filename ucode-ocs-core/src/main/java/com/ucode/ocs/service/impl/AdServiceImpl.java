package com.ucode.ocs.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ucode.ocs.mapper.AdMapper;
import com.ucode.ocs.model.Ad;
import com.ucode.ocs.service.AdService;
import com.ucode.ocs.vo.AdVo;
import com.ucode.tool.base.Paginator;
import com.ucode.tool.util.IdentifyUtils;

@Service
public class AdServiceImpl implements AdService {

    @Autowired
    private AdMapper adMapper;
    
    @Override
    public int insert(Ad ad) {
        Date now = new Date();
        ad.setId(IdentifyUtils.getDefaultSnowflakeId());
        ad.setCreatedTime(now);
        ad.setUpdatedTime(now);
        return adMapper.insert(ad);
    }

    @Override
    public int update(Ad ad) {
        return adMapper.update(ad);
    }

    @Override
    public int deleteById(Long id) {
        return adMapper.deleteById(id);
    }

    @Override
    public Ad findById(Long id) {
        return adMapper.findById(id);
    }

    @Override
    public List<Ad> findByIds(List<Long> ids) {
        return adMapper.findByIds(ids);
    }

    @Override
    public List<Ad> findOnlineByPositionId(Long positionId) {
        return adMapper.findOnlineByPositionId(positionId);
    }

    @Override
    public Paginator<AdVo> queryPage(String name, Long positionId, Integer statuz, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AdVo> list = adMapper.queryList(name, positionId, statuz);
        PageInfo<AdVo> pageInfo = new PageInfo<>(list);
        return new Paginator<>(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), list);
    }

}
