package com.ucode.ocs.service;

import java.util.List;
import com.ucode.ocs.model.Ad;
import com.ucode.ocs.vo.AdVo;
import com.ucode.tool.base.Paginator;

public interface AdService {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param ad
     * @return
     */
    public int insert(Ad ad);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param ad
     * @return
     */
    public int update(Ad ad);
    
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public Ad findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<Ad> findByIds(List<Long> ids);
    /**
     * 查询广告位上线的广告
     * @author liliang
     * @date 2020年7月31日
     * @param positionId
     * @return
     */
    public List<Ad> findOnlineByPositionId(Long positionId);
    /**
     * 分页查询
     * @author liliang
     * @date 2020年7月31日
     * @param name
     * @param positionId
     * @param statuz
     * @param pageNum
     * @param pageSize
     * @return
     */
    public Paginator<AdVo> queryPage(String name,Long positionId,Integer statuz,int pageNum, int pageSize);
}
