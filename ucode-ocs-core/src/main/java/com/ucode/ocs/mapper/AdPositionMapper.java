package com.ucode.ocs.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ucode.ocs.model.AdPosition;

/**
 * 广告位Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface AdPositionMapper {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param adPosition
     * @return
     */
    public int insert(AdPosition adPosition);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param adPosition
     * @return
     */
    public int update(AdPosition adPosition);
    
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public AdPosition findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<AdPosition> findByIds(List<Long> ids);
    /**
     * 分页查询
     * @author liliang
     * @date 2020年7月31日
     * @param name
     * @return
     */
    public List<AdPosition> queryList(@Param("name")String name);
}
