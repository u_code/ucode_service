package com.ucode.ocs.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ucode.ocs.model.Ad;
import com.ucode.ocs.vo.AdVo;

/**
 * 广告Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface AdMapper {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param ad
     * @return
     */
    public int insert(Ad ad);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param ad
     * @return
     */
    public int update(Ad ad);
    
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public Ad findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<Ad> findByIds(List<Long> ids);
    /**
     * 查询广告位上线的广告
     * @author liliang
     * @date 2020年7月31日
     * @param positionId
     * @return
     */
    public List<Ad> findOnlineByPositionId(Long positionId);
    /**
     * 分页查询
     * @author liliang
     * @date 2020年7月31日
     * @param name
     * @param positionId
     * @param statuz
     * @return
     */
    public List<AdVo> queryList(@Param("name")String name,@Param("positionId")Long positionId,@Param("statuz")Integer statuz);
}
