package com.ucode.ocs.model;

import com.ucode.tool.base.BaseMode;

/**
 * 广告位
 * @author: liliang
 * @date: 2020年7月31日 上午11:59:52
 */
public class AdPosition extends BaseMode<Long> {
    /**
     * 位置名称
     */
    private String name;
    /**
     * 宽度
     */
    private Integer width;
    /**
     * 高度
     */
    private Integer height;
    /**
     * 备注
     */
    private String remark;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getWidth() {
        return width;
    }
    public void setWidth(Integer width) {
        this.width = width;
    }
    public Integer getHeight() {
        return height;
    }
    public void setHeight(Integer height) {
        this.height = height;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
}
