package com.ucode.ocs.model;

import com.ucode.tool.base.BaseMode;

/**
 * 广告
 * @author: liliang
 * @date: 2020年7月31日 上午11:59:25
 */
public class Ad extends BaseMode<Long> {
    /**
     * 位置Id
     */
    private Long positionId;
    /**
     * 名称
     */
    private String name;
    /**
     * 图片Id
     */
    private Long fileId;
    /**
     * 路由连接
     */
    private String link;
    /**
     * 路由参数
     */
    private String params;
    /**
     * 状态:0上线1下线
     */
    private Integer statuz;
    /**
     * 备注
     */
    private String remark;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Integer getStatuz() {
        return statuz;
    }

    public void setStatuz(Integer statuz) {
        this.statuz = statuz;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
