package com.ucode.ocs.vo;

import com.ucode.ocs.model.Ad;

public class AdVo extends Ad{

    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    
}
