package com.ucode.sys.dictionary;
/**
 * 子系统类型
 * @author liliang
 * @date: 2019年12月6日 上午11:05:25
 */
public enum SubSysType{
    sys(0, "系统设置"),
    crm(1, "CRM系统"),
    oa(2, "OA系统");

    public int code;

    public String message;

    SubSysType(int code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public static SubSysType get(int code){
        for(SubSysType dataPermissionType : values()){
            if(dataPermissionType.code == code){
                return dataPermissionType;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}