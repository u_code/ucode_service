package com.ucode.sys.dictionary;
/**
 * 数据权限类型
 * @author liliang
 * @date: 2019年12月6日 上午11:05:25
 */
public enum DataPermissionType{
    
    myself(1, "管理本人的数据"),
    myself_sub(2, "管理本人与直属下属的数据"),
    dept(3, "管理本部门的数据"),
    dept_child(4, "管理本部门以及所有下级部门的数据(可附带其他部门以及下级部门)"),
//    myself_specify_dept(5, "管理本人以及指定部门的数据"),
//    myself_specify_dept_child(6, "管理本人以及指定部门和其指定部门所有下级部门的数据"),
    all(100, "管理全部的数据");

    public int code;

    public String message;

    DataPermissionType(int code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public static DataPermissionType get(int code){
        for(DataPermissionType dataPermissionType : values()){
            if(dataPermissionType.code == code){
                return dataPermissionType;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}