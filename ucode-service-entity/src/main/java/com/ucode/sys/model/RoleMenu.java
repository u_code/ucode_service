package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 角色菜单关系
 * @author: liliang
 * @date: 2019年12月22日 下午4:57:47
 */
public class RoleMenu extends BaseMode<Long>{

    private static final long serialVersionUID = -4420615468226948924L;

    /**
     * 角色Id
     */
    private Long roleId;
    
    /**
     * 菜单Id
     */
    private Long menuId;
    /**
     * 权限值表达式
     */
    private String permission;
    
    public RoleMenu(){}

    public RoleMenu(Long roleId, Long menuId,String permission) {
        super();
        this.roleId = roleId;
        this.menuId = menuId;
        this.permission = permission;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
    
}
