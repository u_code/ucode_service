package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;
/**
 * 菜单
 * @author: liliang
 * @date: 2019年12月10日 上午11:54:21
 */
@SuppressWarnings("serial")
public class Menu extends BaseMode<Long>{

    /**
     *子系统类型:参考SubSysType
     */
    private Integer subSys;
    /**
     * 父级菜单
     */
    private Long pid;
    /**
     * 名称
     */
    private String name;
    /**
     * 权限值
     */
    private String permission;
    /**
     * 菜单类型1导航2菜单3页签 4按钮
     */
    private Integer menuType;
    /**
     * 菜单说明
     */
    private String remark;
    
    public Menu() {
        super();
    }
    
    public Menu(Long pid,Integer subSys, String name, String permission, Integer menuType, String remark) {
        super();
        this.pid = pid;
        this.subSys = subSys;
        this.name = name;
        this.permission = permission;
        this.menuType = menuType;
        this.remark = remark;
    }
    
    public Integer getSubSys() {
        return subSys;
    }

    public void setSubSys(Integer subSys) {
        this.subSys = subSys;
    }

    public Long getPid() {
        return pid;
    }
    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPermission() {
        return permission;
    }
    public void setPermission(String permission) {
        this.permission = permission;
    }
    public Integer getMenuType() {
        return menuType;
    }
    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
