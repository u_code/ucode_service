package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 角色用户关系
 * @author: liliang
 * @date: 2019年12月22日 下午4:57:47
 */
public class RoleUser extends BaseMode<Long>{

    private static final long serialVersionUID = 385021351977256192L;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 角色Id
     */
    private Long roleId;
    
    public RoleUser(){}

    public RoleUser(Long userId, Long roleId) {
        super();
        this.userId = userId;
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    
}
