package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;
/**
 * 部门、组织机构
 * @author: liliang
 * @date: 2019年12月10日 上午11:47:30
 */
@SuppressWarnings("serial")
public class Dept extends BaseMode<Long>{

    /**
     * 父级
     */
    private Long parentId;
    /**
     * 祖级ID列表
     */
    private String ancestors;
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 在父部门中的次序值,越大越靠后
     */
    private Integer sort;
    
    public Dept(){}
    
    public Dept(Long parentId,String name, String remark, Integer sort) {
        super();
        this.parentId = parentId;
        this.name = name;
        this.remark = remark;
        this.sort = sort;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getSort() {
        return sort;
    }
    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
