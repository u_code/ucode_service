package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 系统操作记录
 * @author: liliang
 * @date: 2019年12月10日 下午4:22:33
 */
public class ActionRecord extends BaseMode<Long>{
    
    private static final long serialVersionUID = 7635779245796370058L;
    /**
     * 操作人
     */
    private Long createUserId;
    /**
     * 操作类型:0->新增1->修改2->删除
     */
    private Integer types;
    /**
     * 菜单Id
     */
    private Long menuId;
    /**
     * 操作对象
     */
    private Long actionId;
    /**
     * 操作内容
     */
    private String content;
    
    
    
    
}
