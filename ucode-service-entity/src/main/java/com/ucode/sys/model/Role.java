package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 功能角色
 * @author: liliang
 * @date: 2019年12月22日 下午4:19:34
 */
@SuppressWarnings("serial")
public class Role extends BaseMode<Long> {

    /**
     * 名称
     */
    private String name;
    /**
     * 所属系统:SubSysType
     */
    private Integer subSys;
    /**
     * 角色代码,自主添加的无需填写
     */
    private String code;
    /**
     * 数据权限类型,DataPermissionType
     */
    private Integer dataType;
    /**
     * 备注
     */
    private String remark;
    
    public Role(){super();}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getSubSys() {
        return subSys;
    }

    public void setSubSys(Integer subSys) {
        this.subSys = subSys;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }
}
