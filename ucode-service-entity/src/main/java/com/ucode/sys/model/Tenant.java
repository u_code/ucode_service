package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 租户
 * @author: liliang
 * @date: 2020年1月13日 下午5:00:57
 */
public class Tenant extends BaseMode<Long>{

    /**
     * 租户编号
     */
    private String code;
    /**
     * 所属企业
     */
    private String company ;
    /**
     * 联系人
     */
    private String contactor ;
    /**
     * 地址
     */
    private String address ;
    /**
     * 联系电话
     */
    private String telephone ;
    /**
     * 租户状态
     */
    private Integer state ;
    /**
     * 租户管理员Id
     */
    private Long adminId;
    
}
