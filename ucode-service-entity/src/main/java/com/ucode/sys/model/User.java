package com.ucode.sys.model;

import java.util.Date;

import com.ucode.tool.base.BaseMode;

/**
 * 系统用户
 * @author: liliang
 * @date: 2019年12月6日 下午3:44:45
 */
public class User extends BaseMode<Long> {
    private static final long serialVersionUID = -4557040676310924004L;
    /**
     * 用户名
     */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 真实姓名
	 */
	private String realname;
	/**
	 * 头像
	 */
	private String headImg;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 员工编号
     */
    private String num;
	/**
	 * 性别：0 未知 1 男 2 女 
	 */
	private Integer sex;
	/**
	 * 状态：0禁用,1正常
	 */
	private Integer statuz;
	/**
	 * 部门Id
	 */
	private Long deptId;
	/**
	 * 职位
	 */
	private String job;
	/**
	 * 直属上级
	 */
	private Long parentId;
	/**
	 * 最后登录时间
	 */
	private Date lastLoginTime;
	/**
	 * 最后登录Ip
	 */
	private String lastLoginIp;
	
	public User(){}
	
    public User(String username, String password, String realname, String headImg, String mobile, String email, String num, Integer sex,
            Integer statuz, Long deptId, String job, Long parentId, Date lastLoginTime, String lastLoginIp) {
        super();
        this.username = username;
        this.password = password;
        this.realname = realname;
        this.headImg = headImg;
        this.mobile = mobile;
        this.email = email;
        this.num = num;
        this.sex = sex;
        this.statuz = statuz;
        this.deptId = deptId;
        this.job = job;
        this.parentId = parentId;
        this.lastLoginTime = lastLoginTime;
        this.lastLoginIp = lastLoginIp;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRealname() {
        return realname;
    }
    public void setRealname(String realname) {
        this.realname = realname;
    }
    public String getHeadImg() {
        return headImg;
    }
    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public Integer getSex() {
        return sex;
    }
    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public Integer getStatuz() {
        return statuz;
    }
    public void setStatuz(Integer statuz) {
        this.statuz = statuz;
    }
    public Long getDeptId() {
        return deptId;
    }
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }
    public String getJob() {
        return job;
    }
    public void setJob(String job) {
        this.job = job;
    }
    public Long getParentId() {
        return parentId;
    }
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    public Date getLastLoginTime() {
        return lastLoginTime;
    }
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public String getLastLoginIp() {
        return lastLoginIp;
    }
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
}
