package com.ucode.sys.model;

import com.ucode.tool.base.BaseMode;

/**
 * 用户授权信息
 * 记录用户可登录方式
 * @author: liliang
 * @date: 2019年12月6日 下午3:44:45
 */
public class UserOauth extends BaseMode<Long> {
    private static final long serialVersionUID = -3919883201357707326L;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 授权类型，参考LoginOauthTypeEnum枚举
     */
    private Integer oauthType;
    /**
     * 第三方用户全局唯一ID(同一个APP，不同应用下打通)
     */
    private String unionid;
    /**
     * 第三方应用用户Id
     */
    private String openid; 
    /**
     * 第三方token
     */
    private String token;
    
}