package com.ucode.sys.vo;

import java.util.ArrayList;
import java.util.List;
import com.ucode.sys.model.Dept;
import com.ucode.tool.tree.INode;


/**
 * 部门树节点
 * @author: liliang
 * @date: 2020年4月28日 上午11:08:38
 */
@SuppressWarnings("serial")
public class DeptTreeNode extends Dept implements INode{
    
    private List<INode> children;
    
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public Long getParentId() {
        return super.getParentId();
    }

    @Override
    public List<INode> getChildren() {
        return this.children;
    }

    public void setChildren(List<INode> children) {
        this.children = children;
    }

    @Override
    public void addChildren(INode node) {
        if(this.children == null)
            this.children = new ArrayList<INode>();
        this.children.add(node);
    }

}
