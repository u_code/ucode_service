package com.ucode.sys.vo;

import java.util.List;

import com.ucode.sys.model.Role;

@SuppressWarnings("serial")
public class RoleVo extends Role {

    private List<Long> menuIds;
    
    public RoleVo(){
        super();
    }

    public RoleVo(Role role,List<Long> menuIds) {
        super();
        this.menuIds = menuIds;
        this.setId(role.getId());
        this.setCreatedTime(role.getCreatedTime());
        this.setUpdatedTime(role.getUpdatedTime());
        this.setDeltag(role.getDataType());
        this.setCode(role.getCode());
        this.setName(role.getName());
        this.setSubSys(role.getSubSys());
        this.setDataType(role.getDataType());
        this.setRemark(role.getRemark());
    }

    public List<Long> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(List<Long> menuIds) {
        this.menuIds = menuIds;
    }
}
