package com.ucode.sys.vo;

import com.ucode.sys.model.User;

@SuppressWarnings("serial")
public class UserVo extends User{

    /**
     * 直属上级名字
     */
    private String parentName;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 授权角色名称
     */
    private String roleNames;
    
    public UserVo() {
        super();
    }
    public String getParentName() {
        //如果上级是超级管理员则表示没有上级
        if(this.getParentId() == 0)
            return null;
        return parentName;
    }
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
    public String getDeptName() {
        return deptName;
    }
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    public String getRoleNames() {
        return roleNames;
    }
    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }
    
}
