package com.ucode.sys.vo;

import java.util.List;
import com.ucode.sys.model.Menu;

import cn.hutool.core.collection.CollUtil;

@SuppressWarnings("serial")
public class MenuTree extends Menu{

    /**
     * 子菜单
     */
    private List<MenuTree> children;
    
    public MenuTree() {
        super();
    }

    public List<MenuTree> getChildren() {
        if(CollUtil.isEmpty(children))
            children = null;
        return children;
    }

    public void setChildren(List<MenuTree> children) {
        this.children = children;
    }
}
