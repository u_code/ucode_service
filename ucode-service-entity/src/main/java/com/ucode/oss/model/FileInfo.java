package com.ucode.oss.model;

import com.ucode.tool.base.BaseMode;

/**
 * 存储文件
 * @author: liliang
 * @date: 2020年6月11日 上午10:22:34
 */
@SuppressWarnings("serial")
public class FileInfo extends BaseMode<Long> {

    /**
     * 源文件名
     */
    private String sourceName;
    /**
     * 上传客户端Id
     */
    public String clientId; 
    /**
     * bucket 名称
     */
    public String bucket;
    /**
     * 文件访问路径
     */
    private String filePath;
    /**
     * 访问前缀
     */
    public String urlprefix;
    /**
     * 是否私有
     */
    public Boolean isPrivate;
    /**
     * 是否图片
     */
    private Boolean isImg;
    /**
     * 文件类型(HTTP Mime-Type/Content-Type)
     */
    private String mimeType;
    /**
     * 文件大小
     */
    private long size;
    /**
     * 存储源
     */
    private String source;
    public String getSourceName() {
        return sourceName;
    }
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getBucket() {
        return bucket;
    }
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
    public String getUrlprefix() {
        return urlprefix;
    }
    public void setUrlprefix(String urlprefix) {
        this.urlprefix = urlprefix;
    }
    public Boolean getIsPrivate() {
        return isPrivate;
    }
    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    public Boolean getIsImg() {
        return isImg;
    }
    public void setIsImg(Boolean isImg) {
        this.isImg = isImg;
    }
    public String getMimeType() {
        return mimeType;
    }
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    public long getSize() {
        return size;
    }
    public void setSize(long size) {
        this.size = size;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
}