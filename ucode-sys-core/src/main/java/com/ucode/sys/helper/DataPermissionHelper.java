package com.ucode.sys.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ucode.sys.dictionary.DataPermissionType;
import com.ucode.sys.model.Menu;
import com.ucode.sys.model.Role;
import com.ucode.sys.model.User;
import com.ucode.sys.service.DeptService;
import com.ucode.sys.service.MenuService;
import com.ucode.sys.service.RoleService;
import com.ucode.sys.service.UserService;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

@Component
public class DataPermissionHelper {
    
    private static Log log = LogFactory.get();

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private DeptService deptService;
    
    /**
     * 获取管理的用户(可以操作那些人的数据)
     * @author liliang
     * @date 2020年2月13日
     * @param userId 用户Id
     * @param permission 业务模块权限值
     * @return 返回null,则代表管理全部用户，反之则返回管理的目标用户
     */
    public List<Long> findUserManageUsers(Long userId,String permission){
        Menu menu = menuService.findByPermission(permission);
        User user = null;
        if(menu == null || (user = userService.findById(userId)) == null){
            log.info("请检查菜单是否配置permission:{}",permission);
            return new ArrayList<Long>();
        }
        
        List<Role> roles = roleService.findDataRoles(userId, menu.getSubSys(), menu.getPermission());
        if(CollUtil.isEmpty(roles))
            return new ArrayList<Long>();
        Role dataRole = roles.get(0);//上面的查询结果已经按照数据权限级别进行降序处理，因此这里取第一个就是优先级最大的数据权限角色
        
        List<Long> userIds = null;
        switch (DataPermissionType.get(dataRole.getDataType())) {
        case myself:
            //本人
            userIds = new ArrayList<Long>();
            userIds.add(userId);
            break;
        case myself_sub:
            //本人及直属下级
            userIds = userService.findDirectSubordinateUserIds(userId);
            if(userIds == null)
                userIds = new ArrayList<Long>();
            userIds.add(userId);
            break;
        case dept:
            //查询本部门的人员
            userIds = userService.findDeptDirectUserIds(Arrays.asList(user.getDeptId()));
            break;
        case dept_child:
            //查询本部门及下属部门的人员
            userIds = userService.findDeptAllUserIds(user.getDeptId());
            break;
//        case myself_specify_dept:
//            //本人以及指定部门
//            if(StrUtil.isNotBlank(deptIds)){
//                String[] s = deptIds.split(",");
//                List<Long> ids = new ArrayList<Long>();
//                for (int i = 0; i < s.length; i++) {
//                    ids.add(Long.valueOf(s[i].trim()));
//                }
//                userIds = userService.findUsersByDepts(ids);
//            }
//            if(userIds == null){
//                userIds = new ArrayList<Long>();
//                userIds.add(user.getId());
//            }else if(!userIds.contains(user.getId()))//如果不包含本人,则需要添加本人
//                userIds.add(user.getId());
//            break;
//        case myself_specify_dept_child:
//            //多部门及下属部门
//            if(StrUtil.isNotBlank(deptIds)){
//                String[] s = deptIds.split(",");
//                List<Long> ids = new ArrayList<Long>();
//                for (int i = 0; i < s.length; i++) {
//                    ids.add(Long.valueOf(s[i].trim()));
//                }
//                userIds = userService.findUsersByDeptChilds(ids);
//            }
//            
//            if(userIds == null){
//                userIds = new ArrayList<Long>();
//                userIds.add(user.getId());
//            }else if(!userIds.contains(user.getId()))//如果不包含本人,则需要添加本人
//                userIds.add(user.getId());
//            
//            break;
        case all:
            //全部
            break;
        default:
            userIds = new ArrayList<Long>();
            break;
        }
        
        return userIds;
    }
}
