package com.ucode.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ucode.sys.model.RoleUser;
import com.ucode.tool.base.MapVo;

/**
 * 角色用户关系Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface RoleUserMapper {

    /**
     * 批量添加
     * @author liliang
     * @date 2019年12月22日
     * @param list
     * @return
     */
    public int batchInsert(List<RoleUser> list);
    
    /**
     * 删除用户全部角色
     * @author liliang
     * @date 2019年12月22日
     * @param userId
     * @return
     */
    public int deleteByUserId(Long userId);
    
    /**
     * 删除角色关联的用户
     * @author liliang
     * @date 2019年12月22日
     * @param userId
     * @return
     */
    public int deleteByRoleIds(List<Long> roleIds);
    
    /**
     * 删除用户角色
     * @author liliang
     * @date 2020年5月2日
     * @param userIds
     * @param roleId
     * @return
     */
    public int delete(@Param("userIds")List<Long> userIds,@Param("roleId")Long roleId);
    
    /**
     * 查询用户角色Ids
     * @author liliang
     * @date 2019年12月22日
     * @param userId
     * @return
     */
    public List<Long> findRoleIdsByUserId(Long userId);
    
    /**
     * 根据用户分组查询用户关联的角色名称
     * @author liliang
     * @date 2020年4月28日
     * @param userIds
     * @return
     */
    public List<MapVo<Long, String>> findUserRoleNames(List<Long> userIds);
}
