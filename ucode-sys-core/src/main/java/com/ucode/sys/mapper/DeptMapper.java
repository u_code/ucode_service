package com.ucode.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ucode.sys.model.Dept;
import com.ucode.sys.vo.DeptTreeNode;

/**
 * 部门Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface DeptMapper {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param dept
     * @return
     */
    public int insert(Dept dept);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param dept
     * @return
     */
    public int update(Dept dept);
    /**
     * 更新部门祖级Id列表
     * @author liliang
     * @date 2020年5月6日
     * @param depts
     * @return
     */
    public int updateAncestors(@Param("depts") List<Dept> depts);
    
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public Dept findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<Dept> findByIds(List<Long> ids);
    /**
     * 查询全部部门树节点
     * @author liliang
     * @date 2020年5月6日
     * @return
     */
    public List<DeptTreeNode> findAllTreeNodes();
    /**
     * 查询直属下级部门总数
     * @author liliang
     * @date 2020年5月6日
     * @param id
     * @return
     */
    public int findDirectChildCount(Long id);
    /**
     * 查询直属下级部门
     * @author liliang
     * @date 2020年4月27日
     * @param id
     * @return
     */
    public List<Dept> findDirectChilds(Long id);
    /**
     * 查询全部下级部门(多级)-> 存储过程实现
     * 存储过程里用临时表递归实现，因此只返回主键,
     * @author liliang
     * @date 2020年5月6日
     * @param id
     * @return
     */
    public List<Long> findAllChildsBySP(Long id);
    
    /**
     * 查询全部下级部门(多级)-> find_in_set函数实现
     * @author liliang
     * @date 2020年5月6日
     * @param id
     * @return
     */
    public List<Long> findAllChildsByFIS(Long id);
    /**
     * 查询全部下级部门(多级)->ancestors like '1,2%' 模糊查询实现
     * @author liliang
     * @date 2020年5月6日
     * @param ancestorsPrefix
     * @return
     */
    public List<Long> findAllChildsByAPL(@Param("ancestorsPrefix")String ancestorsPrefix);
    
}
