package com.ucode.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ucode.sys.model.Menu;
import com.ucode.sys.vo.MenuTree;

/**
 * 菜单Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface MenuMapper {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月16日
     * @param menu
     * @return
     */
    public int insert(Menu menu);
    
    /**
     * 更新
     * @author liliang
     * @date 2020年1月16日
     * @param menu
     * @return
     */
    public int update(Menu menu);
    
    /**
     * 删除
     * @author liliang
     * @date 2020年1月16日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 删除
     * @author liliang
     * @date 2020年1月16日
     * @param ids
     * @return
     */
    public int delete(List<Long> ids);
    
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月16日
     * @param id
     * @return
     */
    public Menu findById(Long id);
    
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年1月16日
     * @param ids
     * @return
     */
    public List<Menu> findByIds(List<Long> ids);
    
    /**
     * 根据权限值查询
     * @author liliang
     * @date 2020年4月27日
     * @param permission
     * @return
     */
    public Menu findByPermission(String permission);
    
    /**
     * 查询子菜单
     * @author liliang
     * @date 2020年4月27日
     * @param id
     * @return
     */
    public List<MenuTree> findMenuChildren(Long id);
    /**
     * 递归查询子系统的树形结构菜单
     * @author liliang
     * @date 2020年4月27日
     * @param subSys
     * @return
     */
    public List<MenuTree> findMenuTreeList(Integer subSys);
    
    /**
     * 查询全部
     * @author liliang
     * @date 2019年8月2日
     * @return
     */
    public List<Menu> findAll();
    
    /**
     * 条件查询
     * @author liliang
     * @date 2020年4月27日
     * @param subSys
     * @param pid
     * @param name
     * @param permission
     * @param menuType
     * @return
     */
    public List<Menu> queryList(@Param("subSys")Integer subSys,@Param("pid")Long pid, @Param("name")String name, @Param("permission")String permission, @Param("menuType")Integer menuType);
    
}
