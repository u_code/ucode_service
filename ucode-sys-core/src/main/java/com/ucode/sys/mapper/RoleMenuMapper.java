package com.ucode.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ucode.sys.model.RoleMenu;

/**
 * 角色菜单关系Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface RoleMenuMapper {

    public int batchInsert(List<RoleMenu> list);
    
    public int updatePermission(@Param("menuId")Long menuId, @Param("permission")String permission);
    
    public List<Long> findIdsByMenuIds(List<Long> menuIds);
    
    public List<Long> findMenuIdsByRoleId(Long roleId);
    
    public List<RoleMenu> findListByRoleId(Long roleId);
    
    public List<RoleMenu> findListByRoleIds(List<Long> roleIds);
    
    public List<RoleMenu> findByIds(List<Long> ids);
    
    public int deleteByIds(List<Long> ids);
    
    public int deleteByRoleId(List<Long> roleIds);

}
