package com.ucode.sys.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ucode.sys.model.User;
import com.ucode.sys.vo.UserVo;

/**
 * 系统用户Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface UserMapper {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param user
     * @return
     */
    public int insert(User user);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param user
     * @return
     */
    public int update(User user);
    /**
     * 更新密码
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @param password
     * @return
     */
    public int updatePassword(@Param("id")Long id,@Param("password")String password);
    
    /**
     * 更新密码
     * @author liliang
     * @date 2020年1月15日
     * @param ids
     * @param password
     * @return
     */
    public int batchUpdatePassword(@Param("ids")List<Long> ids,@Param("password")String password);
    /**
     * 重置账号
     * @author liliang
     * @date 2020年5月5日
     * @param id
     * @param username
     * @param password
     * @return
     */
    public int resetUsername(@Param("id")Long id, @Param("username")String username, @Param("password")String password);
    /**
     * 更新状态
     * @author liliang
     * @date 2020年5月5日
     * @param ids
     * @param statuz
     * @return
     */
    public int updateStatuz(@Param("ids")List<Long> ids ,@Param("statuz")Integer statuz);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public User findById(Long id);
    
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年1月15日
     * @param ids
     * @return
     */
    public List<User> findByIds(List<Long> ids);
    
    /**
     * 手机号查询
     * @author liliang
     * @date 2020年1月15日
     * @param mobile
     * @return
     */
    public User findByMobile(String mobile);
    /**
     * 邮箱查询
     * @author liliang
     * @date 2020年1月15日
     * @param email
     * @return
     */
    public User findByEmail(String email);
    /**
     * 用户名查询
     * @author liliang
     * @date 2020年1月15日
     * @param username
     * @return
     */
    public User findByUsername(String username);
    /**
     * 根据手机号、邮箱、账号任意一项匹配会员
     * @author liliang
     * @date 2019年12月11日
     * @param mea 手机号或邮箱或账号
     * @return
     */
    public User findByMEA(String mea);
    
    /**
     * 查询直属下级用户IDS
     * @author liliang
     * @date 2020年2月10日
     * @param userId
     * @return
     * @desc
     */
    public List<Long> findDirectSubordinateUserIds(Long userId);
    /**
     * 查询部门直属用户总数
     * @author liliang
     * @date 2020年5月7日
     * @param deptId
     * @return
     */
    public int findDeptDirectUserCount(Long deptId);
    
    /**
     * 查询部门直属用户IDS
     * @author liliang
     * @date 2020年5月7日
     * @param deptIds
     * @return
     */
    public List<Long> findDeptDirectUserIds(List<Long> deptIds);
    
    /**
     * 查询部门以及全部子部门用户IDS
     * @author liliang
     * @date 2020年5月7日
     * @param deptIds
     * @param ancestorsPrefix
     * @return
     */
    public List<Long> findDeptAndAllChildsUserIds(@Param("deptId")Long deptId,@Param("ancestorsPrefix")String ancestorsPrefix);
    
    /**
     * 分页查询
     * @author liliang
     * @date 2020年4月28日
     * @param realname
     * @param username
     * @param mobile
     * @param deptId
     * @param statuz
     * @return
     */
    public List<UserVo> queryList(@Param("realname")String realname, @Param("username")String username, @Param("mobile")String mobile, @Param("deptId")Long deptId, @Param("statuz")Integer statuz);
    
    /**
     * 分页查询角色授权用户
     * @author liliang
     * @date 2020年5月2日
     * @param roleId
     * @param realname
     * @return
     */
    public List<UserVo> queryRoleUserList(@Param("roleId")Long roleId, @Param("realname")String realname);
    
}
