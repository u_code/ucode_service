package com.ucode.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ucode.sys.model.Role;

/**
 * 角色Mapper
 * @author: liliang
 * @date: 2019年12月22日 下午3:50:12
 */
public interface RoleMapper {
    /**
     * 添加
     * @author liliang
     * @date 2020年4月27日
     * @param role
     * @return
     */
    public int insert(Role role);
    /**
     * 更新
     * @author liliang
     * @date 2020年4月27日
     * @param role
     * @return
     */
    public int update(Role role);
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param ids
     * @return
     */
    public int delete(List<Long> ids);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年4月27日
     * @param id
     * @return
     */
    public Role findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月27日
     * @param ids
     * @return
     */
    public List<Role> findByIds(List<Long> ids);
    /**
     * 查询用户对目标模块数据操作权限
     * @author liliang
     * @date 2020年4月27日
     * @param userId 用户Id
     * @param subSys 子系统，这里主要是查询用户是否拥有平台管理员、业务子系统管理员权限
     * @param permission 模块权限值
     * @return
     */
    public List<Role> findDataRoles(@Param("userId")Long userId,@Param("subSys")Integer subSys,@Param("permission")String permission);
    /**
     * 分页查询
     * @author liliang
     * @date 2020年4月27日
     * @param subSys
     * @param name
     * @return
     */
    public List<Role> queryList(@Param("subSys")Integer subSys,@Param("name")String name);
    
    
}
