package com.ucode.sys.service;

import java.util.List;

/**
 * 用户监控service
 * @author: liliang
 * @date: 2019年12月16日 上午11:02:45
 */
public interface UserMonitorService {

    /**
     * 上线
     * @author liliang
     * @date 2019年12月28日
     * @param id
     */
    public void online(Long id);
    
    /**
     * 踢出在线用户
     * @author liliang
     * @date 2019年12月28日
     * @param ids 用户Ids
     */
    public void kickout(List<Long> ids);
    
    
//    /**
//     * 分页获取指定格式key，使用 scan 命令代替 keys 命令，在大数据量的情况下可以提高查询效率
//     *
//     * @param patternKey  key格式
//     * @param currentPage 当前页码
//     * @param pageSize    每页条数
//     * @return 分页获取指定格式key
//     */
//    public PageResult<String> findKeysForPage(String patternKey, int currentPage, int pageSize) {
//        ScanOptions options = ScanOptions.scanOptions()
//                .match(patternKey)
//                .build();
//        RedisConnectionFactory factory = stringRedisTemplate.getConnectionFactory();
//        RedisConnection rc = factory.getConnection();
//        Cursor<byte[]> cursor = rc.scan(options);
//
//        List<String> result = Lists.newArrayList();
//
//        long tmpIndex = 0;
//        int startIndex = (currentPage - 1) * pageSize;
//        int end = currentPage * pageSize;
//        while (cursor.hasNext()) {
//            String key = new String(cursor.next());
//            if (tmpIndex >= startIndex && tmpIndex < end) {
//                result.add(key);
//            }
//            tmpIndex++;
//        }
//
//        try {
//            cursor.close();
//            RedisConnectionUtils.releaseConnection(rc, factory);
//        } catch (Exception e) {
//            log.warn("Redis连接关闭异常，", e);
//        }
//
//        return new PageResult<>(result, tmpIndex);
//    }

}
