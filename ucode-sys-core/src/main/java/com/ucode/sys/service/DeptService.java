package com.ucode.sys.service;

import java.util.List;

import com.ucode.sys.model.Dept;
import com.ucode.sys.vo.DeptTreeNode;

/**
 * 部门service
 * @author: liliang
 * @date: 2019年12月16日 上午11:02:45
 */
public interface DeptService {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param dept
     * @return
     */
    public Dept insert(Dept dept);
    /**
     * 更新
     * @author liliang
     * @date 2020年1月15日
     * @param dept
     * @return
     */
    public int update(Dept dept);
    /**
     * 删除,网关层校验如果有下级部门,或者有关联用户无法删除
     * @author liliang
     * @date 2020年5月4日
     * @param id
     * @return
     */
    public int delete(Long id);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public Dept findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<Dept> findByIds(List<Long> ids);
    
    /**
     * 查询部门树
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<DeptTreeNode> findTree();
    
    /**
     * 查询直属下级部门总数
     * @author liliang
     * @date 2020年5月6日
     * @param id
     * @return
     */
    public int findDirectChildCount(Long id);
    
    /**
     * 查询直属下级部门
     * @author liliang
     * @date 2020年4月27日
     * @param id
     * @return
     */
    public List<Dept> findDirectChilds(Long id);
    
    /**
     * 查询全部有效下级部门
     * @author liliang
     * @date 2020年1月15日
     * @param id 部门Id
     * @return
     */
    public List<Long> findAllChilds(Long id);
    
}
