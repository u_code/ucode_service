package com.ucode.sys.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ucode.sys.mapper.MenuMapper;
import com.ucode.sys.mapper.RoleMapper;
import com.ucode.sys.mapper.RoleMenuMapper;
import com.ucode.sys.mapper.RoleUserMapper;
import com.ucode.sys.model.Menu;
import com.ucode.sys.service.MenuService;
import com.ucode.sys.vo.MenuTree;
import com.ucode.tool.base.Paginator;
import com.ucode.tool.util.IdentifyUtils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    
    @Autowired
    private RoleMapper roleMapper;
    
    @Autowired
    private RoleUserMapper roleUserMapper;
    
    @Autowired
    private RoleMenuMapper roleMenuMapper;
    
    @Override
    public Menu insert(Menu menu) {
        menu.setId(IdentifyUtils.getDefaultSnowflakeId());
        Date now = new Date();
        menu.setCreatedTime(now);
        menu.setUpdatedTime(now);
        int i = menuMapper.insert(menu);
        return i > 0 ? menu : null;
    }

    @Override
    @Transactional
    public int update(Menu menu) {
        Menu dbmenu = menuMapper.findById(menu.getId());
        if(dbmenu != null){
            if(StrUtil.isNotBlank(menu.getPermission()) && !dbmenu.getPermission().equals(menu.getPermission())){
                //这里要级联更新菜单与角色关系表冗余的权限值
                roleMenuMapper.updatePermission(dbmenu.getId(), menu.getPermission());
            }
            return menuMapper.update(menu);
        }
        return 0;
    }

    @Override
    @Transactional
    public int deleteById(Long id) {
        int i = menuMapper.deleteById(id);
        if(i> 0){
            List<Long> ids = roleMenuMapper.findIdsByMenuIds(Arrays.asList(id));
            if(CollUtil.isNotEmpty(ids))
                roleMenuMapper.deleteByIds(ids);
        }
        return i;
    }
    
    @Override
    @Transactional
    public int delete(List<Long> ids) {
        int i = menuMapper.delete(ids);
        if(i> 0){
            List<Long> _ids = roleMenuMapper.findIdsByMenuIds(ids);
            if(CollUtil.isNotEmpty(_ids))
                roleMenuMapper.deleteByIds(_ids);
        }
        return i;
    }

    @Override
    public Menu findById(Long id) {
        return menuMapper.findById(id);
    }
    

    @Override
    public Menu findByPermission(String permission) {
        return menuMapper.findByPermission(permission);
    }

    @Override
    public List<MenuTree> findMenuTreeList(Integer subSys) {
        return menuMapper.findMenuTreeList(subSys);
    }

    @Override
    public Paginator<Menu> queryPage(Integer subSys,Long pid,String name,String permission,Integer menuType,int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Menu> menus = menuMapper.queryList(subSys, pid, name, permission, menuType);
        PageInfo<Menu> pageInfo = new PageInfo<>(menus);
        return new Paginator<>(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), menus);
    }

}
