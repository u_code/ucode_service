package com.ucode.sys.service;

import java.util.List;
import com.ucode.sys.model.Role;
import com.ucode.sys.model.RoleMenu;
import com.ucode.tool.base.MapVo;
import com.ucode.tool.base.Paginator;

/**
 * 角色service
 * @author: liliang
 * @date: 2019年12月16日 上午11:02:45
 */
public interface RoleService {

    /**
     * 添加
     * @author liliang
     * @date 2020年4月28日
     * @param role
     * @return
     */
    public Role insert(Role role);
    /**
     * 更新
     * @author liliang
     * @date 2020年4月28日
     * @param role
     * @return
     */
    public int update(Role role);
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param id
     * @return
     */
    public int deleteById(Long id);
    /**
     * 删除
     * @author liliang
     * @date 2020年5月3日
     * @param ids
     * @return
     */
    public int delete(List<Long> ids);
    /**
     * 更新角色权限菜单
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @param menuIds
     * @return
     */
    public int updateRoleMenus(Long roleId,List<Long> menuIds);
    /**
     * 角色关联用户
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @param userIds
     * @return
     */
    public int relatedRoleUsers(Long roleId,List<Long> userIds);
    /**
     * 取消角色关联用户
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @param userIds
     * @return
     */
    public int unRelatedRoleUsers(Long roleId,List<Long> userIds);
    /**
     * 用户关联角色
     * @author liliang
     * @date 2020年4月28日
     * @param userId
     * @param roleIds
     * @return
     */
    public int relatedUserRoles(Long userId,List<Long> roleIds);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年4月28日
     * @param id
     * @return
     */
    public Role findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年4月28日
     * @param ids
     * @return
     */
    public List<Role> findByIds(List<Long> ids);
    /**
     * 按照优先级查询角色数据权限
     * @author liliang
     * @date 2020年4月28日
     * @param userId
     * @param subSys
     * @param permission
     * @return
     */
    public List<Role> findDataRoles(Long userId,Integer subSys,String permission);
    /**
     * 查询用户角色
     * @author liliang
     * @date 2020年4月28日
     * @param userId
     * @return
     */
    public List<Role> findRolesByUserId(Long userId);
    /**
     * 查询角色关联的菜单主键
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @return
     */
    public List<Long> findMenuIdsByRoleId(Long roleId);
    /**
     * 查询角色关联的权限菜单
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @return
     */
    public List<RoleMenu> findListByRoleId(Long roleId);
    /**
     * 查询角色关联的权限菜单
     * @author liliang
     * @date 2020年4月28日
     * @param roleIds
     * @return
     */
    public List<RoleMenu> findListByRoleIds(List<Long> roleIds);
    /**
     * 根据用户分组查询用户关联的角色名称
     * @author liliang
     * @date 2020年4月28日
     * @param userIds
     * @return
     */
    public List<MapVo<Long, String>> findUserRoleNames(List<Long> userIds);
    /**
     * 分页查询
     * @author liliang
     * @date 2020年4月28日
     * @param subSys
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    public Paginator<Role> queryPage(Integer subSys,String name,int pageNum, int pageSize);

}
