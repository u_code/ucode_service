package com.ucode.sys.service;

import java.util.List;
import com.ucode.sys.model.User;
import com.ucode.sys.vo.UserVo;
import com.ucode.tool.base.Paginator;

/**
 * 系统用户service
 * @author: liliang
 * @date: 2019年12月16日 上午11:02:45
 */
public interface UserService {

    /**
     * 添加
     * @author liliang
     * @date 2020年1月15日
     * @param user 用户信息
     * @return
     */
    public User insert(User user);
    /**
     * 更新用户信息
     * @author liliang
     * @date 2020年1月15日
     * @param user 用户信息
     * @return
     */
    public int update(User user);
    /**
     * 重置登录账号
     * @author liliang
     * @date 2020年5月5日
     * @param id
     * @param username
     * @param password
     * @return
     */
    public int resetUsername(Long id,String username,String password);
    /**
     * 更新密码
     * @author liliang
     * @date 2020年1月15日
     * @param id 主键
     * @param password 新密码
     * @return
     */
    public int updatePassword(Long id,String password);
    /**
     * 更新密码
     * @author liliang
     * @date 2020年5月5日
     * @param ids
     * @param password
     * @return
     */
    public int updatePassword(List<Long> ids ,String password);
    /**
     * 更新状态
     * @author liliang
     * @date 2020年5月5日
     * @param ids
     * @param statuz
     * @return
     */
    public int updateStatuz(List<Long> ids ,Integer statuz);
    /**
     * 主键查询
     * @author liliang
     * @date 2020年1月15日
     * @param id
     * @return
     */
    public User findById(Long id);
    /**
     * 主键集合查询
     * @author liliang
     * @date 2020年3月11日
     * @param ids
     * @return
     */
    public List<User> findByIds(List<Long> ids);
    /**
     * 手机号查询
     * @author liliang
     * @date 2020年1月15日
     * @param mobile
     * @return
     */
    public User findByMobile(String mobile);
    /**
     * 邮箱查找
     * @author liliang
     * @date 2020年1月15日
     * @param email
     * @return
     */
    public User findByEmail(String email);
    /**
     * 用户名查询
     * @author liliang
     * @date 2020年1月15日
     * @param username
     * @return
     */
    public User findByUsername(String username);
    
    /**
     * 根据手机号、邮箱、账号任意一项匹配会员
     * @author liliang
     * @date 2019年12月11日
     * @param mea 手机号或邮箱或账号
     * @return
     */
    public User findByMEA(String mea);
    
    /**
     * 查询直属下级用户IDS
     * @author liliang
     * @date 2020年2月10日
     * @param userId
     * @return
     * @desc
     */
    public List<Long> findDirectSubordinateUserIds(Long userId);
    
    /**
     * 查询部门直属用户总数
     * @author liliang
     * @date 2020年5月7日
     * @param deptId
     * @return
     */
    public int findDeptDirectUserCount(Long deptId);
    
    /**
     * 查询部门直属用户IDS
     * @author liliang
     * @date 2020年5月7日
     * @param deptIds
     * @return
     */
    public List<Long> findDeptDirectUserIds(List<Long> deptIds);
    /**
     * 查询部门以及全部子部门用户IDS
     * @author ll
     * @date 2020年5月7日
     * @param deptIds
     * @return
     * @desc
     */
    public List<Long> findDeptAllUserIds(Long deptId);
    
    /**
     * 分页查询
     * @author liliang
     * @date 2020年4月28日
     * @param realname
     * @param username
     * @param mobile
     * @param deptId
     * @param statuz
     * @param pageNum
     * @param pageSize
     * @return
     */
    public Paginator<UserVo> queryPage(String realname,String username,String mobile,Long deptId,Integer statuz,int pageNum, int pageSize);
   
    /**
     * 分页查询角色授权用户
     * @author liliang
     * @date 2020年4月28日
     * @param roleId
     * @param realname
     * @param pageNum
     * @param pageSize
     * @return
     */
    public Paginator<UserVo> queryPageRoleUsers(Long roleId,String realname,int pageNum, int pageSize);
}
