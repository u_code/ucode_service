package com.ucode.sys.service;

import java.util.List;
import com.ucode.sys.model.Menu;
import com.ucode.sys.vo.MenuTree;
import com.ucode.tool.base.Paginator;

/**
 * 后台菜单service
 * @author: liliang
 * @date: 2019年12月16日 上午11:02:45
 */
public interface MenuService {

    public Menu insert(Menu menu);
    
    public int update(Menu menu);
    
    public int deleteById(Long id);
    
    public int delete(List<Long> ids);
    
    public Menu findById(Long id);
    
    public Menu findByPermission(String permission);
    
    public List<MenuTree> findMenuTreeList(Integer subSys);
    
    public Paginator<Menu> queryPage(Integer subSys,Long pid,String name,String permission,Integer menuType,int pageNum, int pageSize);
}
