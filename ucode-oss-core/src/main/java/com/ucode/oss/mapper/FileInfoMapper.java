package com.ucode.oss.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ucode.oss.model.FileInfo;

/**
 * 文件信息数据
 * @author: liliang
 * @date: 2020年6月11日 上午10:26:48
 */
public interface FileInfoMapper {
    
    public FileInfo findById(Long id);
    
    public List<FileInfo> findByIds(List<Long> ids);
    
    public int insert(FileInfo fileInfo);
    
    public List<FileInfo> queryList(@Param("name")String name,@Param("source")String source);
}
