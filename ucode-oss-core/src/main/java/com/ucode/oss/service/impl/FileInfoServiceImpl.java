package com.ucode.oss.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ucode.oss.mapper.FileInfoMapper;
import com.ucode.oss.model.FileInfo;
import com.ucode.oss.service.FileInfoService;
import com.ucode.tool.base.Paginator;

@Service
public class FileInfoServiceImpl implements FileInfoService {

    @Autowired
    private FileInfoMapper fileInfoMapper;
    
    @Override
    public FileInfo findById(Long id) {
        return fileInfoMapper.findById(id);
    }
    
    @Override
    public List<FileInfo> findByIds(List<Long> ids) {
        return fileInfoMapper.findByIds(ids);
    }

    @Override
    public int insert(FileInfo fileInfo) {
        Date now = new Date();
        fileInfo.setCreatedTime(now);
        fileInfo.setUpdatedTime(now);
        return fileInfoMapper.insert(fileInfo);
    }

    @Override
    public Paginator<FileInfo> queryPage(String sourceName, String source, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<FileInfo> list = fileInfoMapper.queryList(sourceName, source);
        PageInfo<FileInfo> pageInfo = new PageInfo<>(list);
        return new Paginator<>(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), list);
    }

}
