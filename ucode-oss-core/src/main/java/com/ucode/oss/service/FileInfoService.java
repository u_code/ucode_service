package com.ucode.oss.service;

import java.util.List;

import com.ucode.oss.model.FileInfo;
import com.ucode.tool.base.Paginator;

/**
 * 文件信息service
 * @author: liliang
 * @date: 2020年6月11日 上午10:29:04
 */
public interface FileInfoService {

    public FileInfo findById(Long id);
    
    public List<FileInfo> findByIds(List<Long> ids);
    
    public int insert(FileInfo fileInfo); 
    
    public Paginator<FileInfo> queryPage(String sourceName,String source,int pageNum, int pageSize);
}